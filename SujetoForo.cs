using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    class SujetoForo : ISujeto
    {
        //Creamos una interfas para ir agregando los metodos
        private List<IObservador> suscriptores = new List<IObservador>();
        private string mensaje;


        
        public void agregar(IObservador suscriptor)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" agregar un nuevo suscriptor");
            suscriptores.Add(suscriptor);
        }

        public void eliminar(IObservador suscriptor)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("elimina a el suscriptor");
            suscriptores.Remove(suscriptor);
        }

        public void notificar()
        {
            //Utilizamos un foreach que nos permite notificas los cambios
            foreach (IObservador observer in suscriptores)
            {
                observer.Actualizar(mensaje);
            }
        }

        //Utilizamos este metodo para realizar cambios
        public void CambioEstado()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(">>>>>>AÑADIR EL ARTICULO AL FORO<<<<<<<");
            mensaje = "Se procedio un nuevo artículo, revisar para más detalles";
            notificar();
        }
    }
}
