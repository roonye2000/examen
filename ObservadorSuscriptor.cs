using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    class ObservadorSuscriptor : IObservador
    {
        private string nombre;
        private SujetoForo sujeto;

        
        //Procedemos a crear un costructor para el suscriptor 
        public ObservadorSuscriptor(string pNombre, SujetoForo pSujeto)
        {
            nombre = pNombre;
            sujeto = pSujeto;
            
        }

        //Creamos el Método que vamos a implementa para asi actualizar los cambios de estado
        public void Actualizar(string mensaje)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("notificar a=: {0}'- {1}", nombre, mensaje);
        }
    }
}
