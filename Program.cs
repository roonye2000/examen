using System;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Procedemos a crear SujetoForo
            SujetoForo Foro = new SujetoForo();

            //En estas lineas creamos las instancias de ObservadorSuscriptor y lo vamos sumando al foro
            ObservadorSuscriptor suscriptorUno = new ObservadorSuscriptor("Roonye", Foro);
            Foro.agregar(suscriptorUno);
            ObservadorSuscriptor suscriptorDos = new ObservadorSuscriptor("Isabella", Foro);
            Foro.agregar(suscriptorDos);

            //Implemento un ciclo For
            for (int i = 0; i < 2; i++)
            {
                Foro.CambioEstado();
            }

            Console.WriteLine();
            
            Foro.eliminar(suscriptorDos);

            //Aplicamos un ciclo for
            for (int i = 0; i < 2; i++)
            {
                Foro.CambioEstado();
            }

            Console.WriteLine();
            //Procedemos añadir un nuevo Suscriptor
            ObservadorSuscriptor suscriptorTres = new ObservadorSuscriptor("Anthony", Foro);
            Foro.agregar(suscriptorTres);

     
            Foro.CambioEstado();

        }
    }
}
