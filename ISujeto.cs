using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    //En esta parte vamos a proceder a crear la interfas en los que vamos a poder agregar todos los metodos que va a utilizar el observable
    interface ISujeto
    {
        //Va Permite agregar los suscriptores
        void agregar(IObservador suscriptor);
        
        // En este caso va a Elimina los suscriptores
        void eliminar(IObservador suscriptor);
        
        //Este Método nos sirve para notificar los cambios de estado del sujeto
        void notificar();
    }
}
