using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    
    interface IObservador
    {
        //Esto Permite que se reciba las notificaciones sobre los cambios de estado
        void Actualizar(string mensaje);
    }
}
